#!/usr/bin/env php
<?php
/**
 * Test file
 * 
 * @author Pierre HUBERT
 */

require_once __DIR__."/lib/Finder.php";

$finder = new \PierreHubert\CityToPostalCode\Finder(__DIR__."/data");

$city = "Veauche";

if(isset($_SERVER["argv"]) && isset($_SERVER["argv"][1]))
	$city = $_SERVER["argv"][1];

$results = $finder->search($city);

echo "Country\tCode\tCity name\n";
echo "---------------------------------------------\n";
foreach($results as $result)
	echo $result->get_country(),"\t",$result->get_postal_code_str(),"\t",$result->get_city_name(),"\n";
