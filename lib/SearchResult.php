<?php
namespace PierreHubert\CityToPostalCode;

/**
 * Single search result
 * 
 * @author Pierre HUBERT
 */

class SearchResult {
	private $postal_code;
	private $country;
	private $city_name;

	/**
	 * Check if this search result is valid
	 */
	public function isValid() : bool {
		return $this->postal_code != null && $this->postal_code > 0;
	}

	public function set_postal_code(int $postal_code){
		$this->postal_code = $postal_code;
	}

	public function get_postal_code() : int {
		return $this->postal_code;
	}

	public function get_postal_code_str() : string {
		return ($this->postal_code < 10000 ? "0".$this->postal_code : (string)$this->postal_code);
	}

	public function set_country(string $country){
		$this->country = $country;
	}

	public function get_country() : string {
		return $this->country;
	}

	public function set_city_name(string $city_name){
		$this->city_name = $city_name;
	}

	public function get_city_name() : string {
		return $this->city_name;
	}
}