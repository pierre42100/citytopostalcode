<?php
/**
 * Main class of the project
 * 
 * @author Pierre HUBERT
 */

namespace PierreHubert\CityToPostalCode;

require_once __DIR__."/SearchResult.php";

class Finder{

	/**
	 * Data directory
	 */
	private $data_dir;

	/**
	 * Determine available countries
	 */
	private $countries;

	/**
	 * Pointers on files
	 */
	private $files = array();

	/**
	 * Construct a new Finder instance
	 * 
	 * @param $path The path to the the data directory for the library
	 */
	public function __construct(string $path){

		//Save data directory
		$this->data_dir = ($path[strlen($path)-1] == "/" ? $path : $path."/");

		if(!file_exists($this->data_dir))
			throw new Exception("Data directory does not exists! Did your forget to run get data script ?");

		$countries = glob($this->data_dir."*");
		for($i = 0; $i < count($countries); $i++)
			$countries[$i] = str_replace($this->data_dir, "", $countries[$i]);
		$this->countries = $countries;

	}


	public function __destruct(){

		//Close files
		foreach($this->files as $file)
			fclose($file);
	}

	/**
	 * Make a search
	 * 
	 * @param $city The name of the city to search
	 * @param $restrictive Specify whether we will search only for the exact city
	 * name and not try to make larger search
	 * @return array The list of results (a unique result might reveal that we found the right city)
	 */
	public function search(string $city, bool $restrictive = false) : array {

		$list = array();
		
		for($i = 0; $i < count($this->countries) && count($list) == 0; $i++)
			$list = $this->searchCountry($this->countries[$i], $city, $restrictive);

		return $list;
	}


	public function searchCountry(string $country, string $city, bool $restrictive = false) : array {

		//Open list file and search inside of it for the city
		$list_file = $this->data_dir.$country."/list";
		$this->need_file($list_file);
		$file = $this->fopen($list_file);

		$list = array();

		//Process the file line per line
		while(($line = fgets($file)) != FALSE){
			
			//Search for city name
			if(stripos($line, $city) === false)
				continue;

			$info = explode("\t", $line);

			if(
				$info[14] == 0 //Check for population
				|| $info[6] != "P" //Place where people live
			)
				continue;


			//Stop the search if we got the exact match
			$is_exact_match = strcasecmp($this->removeSpecialChars($info[1]), $this->removeSpecialChars($city)) === 0;
			if($restrictive && !$is_exact_match)
				continue;
			
			//Reset search to get only exact matches
			if(!$restrictive && $is_exact_match){
				$restrictive = true;
				$list = array();
			}
			
			if(count($list) > 100)
				continue;

			
			//We add the city to the list

			//Get city postal code
			$postal_code = $this->getPostalCode($country, (int)$info[0] /*city code */);
			
			$result = new SearchResult();
			$result->set_postal_code($postal_code);
			$result->set_country($country);
			$result->set_city_name($info[1]);

			$list[] = $result; //Return only perfect match
		
				
		}

		$this->fclose($file);

		return $list;
	}


	private function getPostalCode(string $country, int $cityID) : int {

		//Open file
		$alternate_names_file = $this->data_dir.$country."/alternate_names";
		$this->need_file($alternate_names_file);
		$file = $this->fopen($alternate_names_file);

		//Process file
		$code = -1;
		while(($line = fgets($file)) != false){
			
			if(strpos($line, "\t".$cityID."\tpost") == false)
				continue;
			
			
			//Extract postal code
			$code = (int)explode("\t", $line)[3];
			break;
		}

		$this->fclose($file);


		return $code;
	}


	private function need_file(string $path) {
		if(!file_exists($path))
			throw new Exception("File ".$path." does not exists!");
	}

	private function fopen(string $path) {

		//Open file if required
		if(!isset($this->files[$path])){

			$file = fopen($path, "ro");
			if($file == FALSE)
				throw new Exception("Could not open ".$list_file. "for reading !");
			$this->files[$path] = $file;
		}

		//Else seek file to the begining
		else {
			$file = $this->files[$path];
			rewind($file);
		}

		return $file;
	}

	private function fclose($file){
		//DO NOTHING
	}



	/**
	 * Taken from https://stackoverflow.com/a/14114419/3781411
	 */
	private function removeSpecialChars(string $string) : string {
		$string = mb_strtolower($string, "UTF-8");
		$string = str_replace(array("é", "è", "ê", "ä"), "e", $string);
		$string = str_replace(array("à", "â", "ä"), "a", $string);
		$string = str_replace(array("ô", "ö"), "o", $string);
		$string = str_replace(array("î", "ï"), "i", $string);
		$string = str_replace(array("û", "ü", "ù"), "u", $string);
		$string = str_replace(array("ç"), "c", $string);
		return $string;
	}
}