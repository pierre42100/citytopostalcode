#!/bin/bash

#
# Get and save data from an online source
# @author Pierre HUBERT
#
mkdir data

process_language (){

	lang_directory=data/$1
	mkdir $lang_directory
	list_file=$lang_directory/list.zip
	alternate_names=$lang_directory/alternateNames.zip

	#Download file
	wget -O $list_file  https://download.geonames.org/export/dump/$2.zip
	wget -O $alternate_names https://download.geonames.org/export/dump/alternatenames/$2.zip


	#Extract them
	unzip $list_file $2.txt -d $lang_directory
	mv $lang_directory/$2.txt $lang_directory/list
	unzip $alternate_names $2.txt -d $lang_directory
	mv $lang_directory/$2.txt $lang_directory/alternate_names

}

#Process french sources
process_language fr FR