# CityToPostalCode

A lightweight PHP library to map a city name to a postal code


## Getting the databases
Point your terminal to the root directory of this project and then run
```sh
./get_data.sh
```

## Testing the project
Use the test.php programm :
```sh
./test.php [city_name]
```

## Adding support for countries different that France
By default, only cities from France are detected. To add support for other countries, edit the file `get_data.sh` and appends line to the file following the pattern

```bash
process_language [country_code] [COUNTRY_CODE]
```
 
with one line per country.


## Integrated in a PHP project
```php
require_once _PATH_TO_LIBRARY_."/lib/Finder.php";

$finder = new \PierreHubert\CityToPostalCode\Finder(_PATH_TO_LIBRARY_."/data");
$results = $finder->search("city_name", /*restrictive mode*/ false);

/**
 * Results will be an empty array if no result could be found
 * otherwise it will contains
 * * All the matches for cities and only the exact matches if at least one was found
 * * The 100 first cities whose name were close to the queried city name
 */

```
